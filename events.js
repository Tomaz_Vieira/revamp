window.onload = function(){

	function percentize(value){
		var s = String(value*100)
		return s[2] == '.' || s[2] == undefined ? String(value*100).slice(0,5) + '%' : '0' + String(value*100).slice(0,4) + '%'
	}

	document.getElementById('caixa_parametros').addEventListener('click', function(e){
		
		if(e.target.className.match(/plus/) ){
			meter = e.target.parentNode.parentNode.querySelectorAll('input')[0]	
			
			// if critical threshold is one lower than difficulty, do nothing
			if (meter.id == 'critica'){
				if( Number(document.getElementById('critica').value) ==  Number(document.getElementById('dificuldade').value) -1 )
					return
			}
			
			if(  Number(meter.value) < 10  ){
				meter.value = Number(meter.value) + 1
				return
			}
		}else if( e.target.className.match(/minus/) ){
			meter = e.target.parentNode.parentNode.querySelectorAll('input')[0]	
			
			// if critical threshold is one lower than difficulty, do nothing
			if (meter.id == 'dificuldade'){
				if( Number(document.getElementById('critica').value) ==  Number(document.getElementById('dificuldade').value) -1 )
					return
			}
			
			if(  Number(meter.value) > 0  ){
				meter.value = Number(meter.value) - 1
				return
			}
		}

	})
	
	document.getElementById('calcular').addEventListener('click', function(e){
		
		var numDados = Number(document.getElementById('numero_dados').value)
		var minSucessos = Number(document.getElementById('sucessos_minimos').value)
		var limiteCritico = Number(document.getElementById('critica').value)
		var dificuldade = Number(document.getElementById('dificuldade').value)
		var resultados = calculaEficiente(numDados, minSucessos, dificuldade, limiteCritico)
		
		document.getElementById('resultados').innerHTML +=
			"<table class=\"results\">" + 
				"<tr><td>Dice to roll:</td><td>" + numDados + "</td></tr>" +
				"<tr><td>Difficulty:</td><td>" + dificuldade + "</td></tr>" +
				"<tr><td>Minimum successes required:</td><td>" + minSucessos + "</td></tr>" + 
				"<tr><td>Likelihood:</td><td>" + percentize(resultados[0]) + "</td></tr>" +
				"<tr><td>Average number of successes:</td><td>" + String(resultados[2]).slice(0,5) + "</td></tr>" +
				"<tr><td>Critical threshold:</td><td>" + limiteCritico + "</td></tr>" +
				"<tr><td>Critical failure likelyhood:</td><td>" + percentize(resultados[1]) + "</td></tr>" +
			"</table>"
		
	})
	
	document.getElementById('limpar').addEventListener('click', function(e){
		
		document.getElementById('resultados').innerHTML = '';
	})
	
	
}