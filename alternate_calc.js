function factorial(x){
	if (x<0)
		throw "you fucked up"

	
	if (x==1 || x==0)
		return 1;
	else	
		return x * factorial(x-1);
}

function power(base, exp){
	var p = 1;
	
	if (exp == 0)
		return 1
	
	if(exp == 1)
		return base
		
	for (i=1; i<=exp; i++){
		p *= base; 
	}
	
	return p;
}

function combination(n, p){
	return factorial(n)/(factorial(p) * factorial(n-p))
}

function getChance(difficulty, tipo){
	if(tipo=="sucesso"){
		return (10-difficulty+1)/10
	}else if(tipo=="critical failure"){
		return difficulty/10
	}
}

function findPairs(numDice, minSuccesses){
	var pairs = [];
	var sucessos_backup
	var criticalFailures = 0;

	while(minSuccesses <= numDice){
		sucessos_backup = minSuccesses;
		criticalFailures = 0;
		while( (minSuccesses + criticalFailures) <= numDice ){
			pairs.push ({s: minSuccesses, fc: criticalFailures})
			
			minSuccesses++;
			criticalFailures++;
		}
	minSuccesses = sucessos_backup;
	minSuccesses++;
	}
	return pairs;
}


function fatorDasCombinacoes(numDice, successes, criticalFailures){
	var fator_sucesso = combination(numDice, successes);
	var fator_falha_critica = combination( (numDice - successes), criticalFailures  )
	
	return fator_sucesso * fator_falha_critica
}

function fatorDasProbabilidades(numDice, successes, difficulty, criticalFailures, criticalThreshold){
	
	var success_chance = getChance(difficulty, 'sucesso')
	var chance_falha_critica = getChance( criticalThreshold, 'critical failure')
	var fator_sucesso = power( success_chance , successes )
	var fator_falhas_criticas = power( chance_falha_critica , criticalFailures)
	var fator_falhas = power ( (1 - success_chance - chance_falha_critica) , (numDice - successes - criticalFailures)  )

	return fator_sucesso * fator_falhas_criticas * fator_falhas
}

function chanceIteration(numDice, successes, difficulty, criticalFailures, criticalThreshold){
	return  fatorDasCombinacoes(numDice, successes, criticalFailures) * fatorDasProbabilidades(numDice, successes, difficulty, criticalFailures, criticalThreshold)

}

function calculaEficiente(numDice, minSuccesses, difficulty, criticalThreshold){
	var pairs = findPairs(numDice, minSuccesses)
	var index = 0;
	var total_chance = 0;
	var chance_critica_total = 0;
	var avg = 0;
	
	for( index=0; index < pairs.length; index++){
		
		total_chance += chanceIteration(numDice, pairs[index].s/*successes*/, difficulty, pairs[index].fc/*criticalFailures*/, criticalThreshold)
	}
	
	for( index=1; index <= numDice; index++){
		//console.log('index: ' + index)
		chance_critica_total += chanceIteration(numDice, 0/*successes*/, difficulty, index/*criticalFailures*/, criticalThreshold)
	}
	
	//now for the average success rate
	pairs = findPairs(numDice, 0)
	for( index=0; index < pairs.length; index++){
		avg += chanceIteration(numDice, pairs[index].s/*successes*/, difficulty, pairs[index].fc/*criticalFailures*/, criticalThreshold) * pairs[index].s
	}
	
	return [total_chance, chance_critica_total, avg]
	
}