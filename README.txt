ReVamp is a webapp designed to help calculate statistics on tabletop RPG dice rolls.
It is further explained within its page. Just open main.html on a browser.

It was tested for gecko and webkit browsers, but the more complex CSS should
degrade gracefully on old (or plain bad) browsers.
